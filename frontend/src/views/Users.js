import React, { useEffect, useState } from 'react';
import {useHistory} from 'react-router-dom'
import { useFormik } from 'formik';
import axios, {getUser} from '../services/api'
import { withSwal } from 'react-sweetalert2';
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CRow, CCol,
  CForm,
  CFormGroup,
  CInput,
  CFormText
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
//import { changeUserInfo } from './Users';


const Users = ({swal}) =>{

  const access_token = localStorage.getItem('access_token')
  const history = useHistory()
  const [details, setDetails] = useState([])
  const [status, setStatus] = useState(0)
  const [userData, setUserData] = useState([])
  const [showModal, setShowModal] = useState(false)
  const [posts, setPosts] = useState([])
  const [rowID, setRowID] =useState(null)
  const [rowData, setRowData] = useState([])
  const [loadId, setLoadId] = useState(1)
  const fields = ['firstName','lastName', 'email', 'phoneNumber', 'role', 'action']

  const hanldeShowModal = () => {
    setRowID(null)
    setShowModal(!showModal)
  }

  const addOrEdit = (id, item) => {
    setRowID(id)
    setRowData(item)
  }

  useEffect(() => {
    if (rowID !== null){
      setShowModal(!showModal)
    }
  }, [rowID])

  function handleAddNew() {
    setStatus(status + 1)
  }

const deletePost = (id) => {
    swal.fire({
      title: 'Êtes-vous sûr ?',
      text: "Vous ne pourrez pas revenir en arrière",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Oui, Supprimer'
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/User/Delete/${id}`)
        .then(() => setLoadId(loadId+1))
        swal.fire(
          'Suppression',
          'Suppression réussie.',
          'success'
        )
      }
    })
  };

 useEffect(() => {
  axios.get('/User/GetUsers', {
    responseType: 'json'
  }).then(response => {
    setUserData(response.data.map(user => {
      return {
        ...user
      }
    }))
  })
 }, [loadId])

  return (
    <>
      <CRow className="justify-content-center">
        <CCol md="12">
          <CCard>
            {fields.includes('action')&&(
              <CCardHeader >
                <CRow>
                  <CCol>
                    <CButton onClick={()=>addOrEdit(-1, [])} className="px-5" color="info">+ Ajouter un Utilisateur</CButton>
                    <Modal rowID={rowID}  display={showModal} rowData={rowData} handleDisplay={hanldeShowModal}  handleAddNew={handleAddNew} refresh={() => setLoadId(loadId+1)} />
                  </CCol>
                </CRow>
              </CCardHeader>
            )}
            <CCardBody>
              <CDataTable
                items={userData}
                fields={fields}
                itemsPerPageSelect
                itemsPerPage={5}
                pagination
                tableFilter
                scopedSlots = {{
                  'firstName' : (item) => (
                    <td>
                      {item.firstName}
                    </td>
                  ),
                  'lastName': (item) => (
                    <td>
                      {item.lastName}
                    </td>
                  ),
                  'email':(item)=>(
                    <td>
                        {item.email}
                    </td>
                  ),
                  'phoneNumber':(item) => (
                    <td>
                        {item.phoneNumber}
                    </td>
                  ),
                  'role':
                    (item)=>(
                      <td>
                          <CBadge shape={'pill'} color={item.role == 'Admin'? 'warning' 
                            : item.role == 'SuperAdmin'? 'danger'
                            : 'primary'}>
                            {item.role == 'Admin'? "Administrateur" 
                              : item.role == 'SuperAdmin'? 'SuperAdmin'
                              : "Utilisateur"}
                          </CBadge>
                      </td>
                    ),
                  'action': (item) => (
                    <td width={102}>
                      <CRow>
                        <CCol>
                            <CButton onClick={(e)=> addOrEdit(item.id, item)} className={'btn-pill'} size={'sm'} ><CIcon className={'cust_action_edit'} name={'cilPencil'} /></CButton>
                        </CCol>
                        <CCol>
                            <CButton onClick={(e) => deletePost(item.id)}  className={'btn-pill'} size={'sm'} ><CIcon className={'cust_action_delete'} name={'cilTrash'}/></CButton>
                        </CCol>
                      </CRow>
                    </td>
                  )
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

const Modal = withSwal ((props) => {
    const {swal} = props
    const [mess, setMess] = useState('')
    const history = useHistory()
    const rowData = props.rowData
    function handleAddNewOne() {
      props.handleAddNew();
    }
    const handleDisplay = () => {
      setMess('')
      props.handleDisplay()
    }
  
    const validate = values => {
      const errors = {};
      values.firstName || (errors.firstName = 'Champ obligatoire');
      values.lastName || (errors.lastName = 'Champ obligatoire');
      values.email || (errors.email = 'Champ obligatoire');
      values.phoneNumber || (errors.phoneNumber = 'Champ obligatoire');
      values.password || (errors.password = 'Champ obligatoire');
      return errors;
    }
    const formik = useFormik({
      enableReinitialize: true,
      initialValues: {
        id: rowData.id,
        firstName: rowData.firstName || '',
        lastName: rowData.lastName || '',
        email: rowData.email || '',
        phoneNumber: rowData.phoneNumber || '',
        password: rowData.password || '',
      },
      validate,
      onSubmit: values => {
        add_user(values)
      }
    })

    async function add_user(values) {
        const access_token = (localStorage.getItem('access_token'))
        if (access_token){
          try {
            if (!values.id){
                axios
                .post(`/User/AddOrUpdate/`, {
                    firstName: values.firstName,
                    lastName: values.lastName, 
                    email: values.email, 
                    phoneNumber: values.phoneNumber, 
                    password: values.password
                })
                .then(response => {
                  handleAddNewOne();
                  handleDisplay();
                  // history.push('/users')
                  props.refresh()
                  swal.fire({
                      title: 'Succès',
                      text: 'Enregistrement avec succès',
                      icon: 'success',
                  });
                }).catch(err => {
                  setMess("Erreur d'enregistrement !")
                });
            }else{
                axios
                .post(`/User/AddOrUpdate/`, {
                    id: values.id,
                    firstName: values.firstName,
                    lastName: values.lastName, 
                    email: values.email, 
                    phoneNumber: values.phoneNumber, 
                    password: values.password 
                })
                .then(response => {
                  handleAddNewOne();
                  handleDisplay();
                  // history.push('/users')
                  props.refresh()
                  swal.fire({
                    title: 'Succès',
                    text: 'Modification avec succès',
                    icon: 'success',
                });
                }).catch(err => {
                  setMess("Erreur d'enregistrement !")
                });
            }
          } catch (e) {
            alert(e.message)
            history.push('/login')
            localStorage.removeItem('access_token')
          }
        }
    
      }
  
    return (
      <>
        <CModal
          show={props.display}
          onClose={(e)=>handleDisplay()}
          size="lg"
          color={'info'}
        >
          <CModalHeader closeButton style={{textAlign: 'center'}}>Enregistrement Utilisateur</CModalHeader>
          <CModalBody>
            <CForm>
              <CFormGroup>
                
                  <CCol>
                    <CInput id="firstName" name="firstName" placeholder="firstName" value={formik.values.firstName} onChange={formik.handleChange} />
                    <p className="text-warning" >{formik.errors.firstName?formik.errors.firstName:null}</p>
                  </CCol>

                  <CCol>
                    <CInput id="lastName" name="lastName" placeholder="lastName" value={formik.values.lastName} onChange={formik.handleChange} />
                    <p className="text-warning" >{formik.errors.lastName?formik.errors.lastName:null}</p>
                  </CCol>

                  <CCol>
                    <CInput id="email" name="email" placeholder="E-mail" value={formik.values.email} onChange={formik.handleChange} />
                    <p className="text-warning" >{formik.errors.email?formik.errors.email:null}</p>
                  </CCol>

                  <CCol>
                    <CInput id="phoneNumber" name="phoneNumber" placeholder="Numéro de téléphone" value={formik.values.phoneNumber} onChange={formik.handleChange} />
                    <p className="text-warning" >{formik.errors.phoneNumber?formik.errors.phoneNumber:null}</p>
                  </CCol>

                  <CCol>
                    <CInput type='password' id="password" name="password" placeholder="Password" value={formik.values.password} onChange={formik.handleChange} />
                    <p className="text-warning" >{formik.errors.password?formik.errors.password:null}</p>
                  </CCol>
                  <CInput type='hidden' id="id" name="id" placeholder="id" value={formik.values.id} onChange={formik.handleChange} />

                <CFormText className="help-block" color={'danger'}>{mess}</CFormText>
              </CFormGroup>
            </CForm>
          </CModalBody>
          <CModalFooter>
            <CButton onClick={formik.handleSubmit} type="submit" color="info">Enregistrement des données</CButton>{' '}
            <CButton
              color="secondary"
              onClick={(e) => handleDisplay()}
            >Fermer</CButton>
          </CModalFooter>
        </CModal>
      </>
    )
  })

export default withSwal (Users)
