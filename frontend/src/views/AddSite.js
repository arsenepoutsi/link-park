import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import {useHistory} from 'react-router-dom'
import axios from '../services/api'
import { withSwal } from 'react-sweetalert2';
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CRow, CCol,
  CForm,
  CFormGroup,
  CInput,
  CFormText,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'


const AddSite = ({swal}) =>{
  const [status, setStatus] = useState(0)
  const [siteData, setSiteData] = useState([])
  const [showModal, setShowModal] = useState(false)
  const [rowID, setRowID] =useState(null)
  const [rowData, setRowData] = useState([])
  const [loadId, setLoadId] = useState(1)
  const fields = ['adresse','ville', 'zipCode', 'tel', 'email','action']

  const hanldeShowModal = () => {
    setRowID(null)
    setShowModal(!showModal)
  }

  const addOrEdit = (id, item) => {
    setRowID(id)
    setRowData(item)
  }

  useEffect(() => {
    if (rowID !== null){
      setShowModal(!showModal)
    }
  }, [rowID])

  function handleAddNew() {
    setStatus(status + 1)
  }

  const deletePost = (id) => {
    swal.fire({
      title: 'Êtes-vous sûr ?',
      text: "Vous ne pourrez pas revenir en arrière",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Oui, Supprimer'
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/Site/Delete/${id}`)
        .then(() => setLoadId(loadId+1))
        swal.fire(
          'Suppression',
          'Suppression réussie.',
          'success'
        )
      }
    })
  };

 useEffect(() => {
  axios.get('/Site/GetAll', {
    responseType: 'json'
  }).then(response => {
    setSiteData(response.data.map(site => {
      return {
        ...site
      }
    }))
  })
 }, [loadId])

  return (
    <>
      <CRow className="justify-content-center">
        <CCol md="12">
          <CCard>
            {fields.includes('action')&&(
              <CCardHeader >
                <CRow>
                  <CCol>
                    <CButton onClick={()=>addOrEdit(-1, [])} className="px-5" color="info">+ Ajouter un Site</CButton>
                    <Modal rowID={rowID}  display={showModal} rowData={rowData} handleDisplay={hanldeShowModal}  handleAddNew={handleAddNew} refresh={() => setLoadId(loadId+1)}/>
                  </CCol>
                </CRow>
              </CCardHeader>
            )}
            <CCardBody>
              <CDataTable
                items={siteData}
                fields={fields}
                itemsPerPageSelect
                itemsPerPage={5}
                pagination
                tableFilter
                scopedSlots = {{
                  'adresse' : (item) => (
                    <td>
                      {item.adresse}
                    </td>
                  ),
                  'ville': (item) => (
                    <td>
                      <b>{item.ville}</b>
                    </td>
                  ),
                  'zipCode':(item)=>(
                    <td>
                        {item.zipCode}
                    </td>
                  ),
                  'tel':(item) => (
                    <td>
                        {item.tel}
                    </td>
                  ),
                  'email':(item) => (
                    <td>
                        {item.email}
                    </td>
                  ),
                  'action': (item) => (
                    <td width={102}>
                      <CRow>
                        <CCol>
                            <CButton onClick={(e)=> addOrEdit(item.id, item)} className={'btn-pill'} size={'sm'} ><CIcon className={'cust_action_edit'} name={'cilPencil'} /></CButton>
                        </CCol>
                        <CCol>
                            <CButton onClick={(e) => deletePost(item.id)}  className={'btn-pill'} size={'sm'} ><CIcon className={'cust_action_delete'} name={'cilTrash'}/></CButton>
                        </CCol>
                      </CRow>
                    </td>
                  )
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

const Modal = withSwal ((props) => {
    const {swal} = props
    const [mess, setMess] = useState('')
    const history = useHistory()
    const rowData = props.rowData
    function handleAddNewOne() {
      props.handleAddNew();
    }
    const handleDisplay = () => {
      setMess('')
      props.handleDisplay()
    }
  
    const validate = values => {
      const errors = {};
      values.adresse || (errors.adresse = 'Required');
      values.ville || (errors.ville = 'Required');
      values.tel || (errors.tel = 'Required');
      values.email || (errors.email = 'Required');
      values.zipCode || (errors.zipCode = 'Required');
      return errors;
    }
    const formik = useFormik({
      enableReinitialize: true,
      initialValues: {
        id: rowData.id,
        adresse: rowData.adresse || '',
        ville: rowData.ville || '',
        tel: rowData.tel || '',
        email: rowData.email || '',
        zipCode: rowData.zipCode || '',
      },
      validate,
      onSubmit: values => {
        add_site(values)
      }
    })

    async function add_site(values) {
        const access_token = (localStorage.getItem('access_token'))
        if (access_token){
          try {
            if (!values.id){
                axios
                .post(`/Site/AddOrUpdate/`, {
                    adresse: values.adresse,
                    ville: values.ville, 
                    zipCode: values.zipCode, 
                    tel: values.tel, 
                    email: values.email
                })
                .then(response => {
                  handleAddNewOne();
                  handleDisplay();
                  // history.push('/sites')
                  props.refresh()
                  swal.fire({
                      title: 'Succès',
                      text: 'Enregistrement avec succès',
                      icon: 'success',
                  });
                }).catch(err => {
                  setMess("Erreur d'enregistrement !")
                });
            }else{
                axios
                .post(`/Site/AddOrUpdate/`, {
                    id: values.id,
                    adresse: values.adresse,
                    ville: values.ville, 
                    zipCode: values.zipCode, 
                    tel: values.tel, 
                    email: values.email 
                })
                .then(response => {
                  handleAddNewOne();
                  handleDisplay();
                  // history.push('/sites')
                  props.refresh()
                  swal.fire({
                      title: 'Succès',
                      text: 'Modification avec succès',
                      icon: 'success',
                  });
                }).catch(err => {
                  setMess("Erreur d'enregistrement !")
                });
            }
          } catch (e) {
            alert(e.message)
            history.push('/login')
            localStorage.removeItem('access_token')
          }
        }
    
      }
  
    return (
      <>
        <CModal
          show={props.display}
          onClose={(e)=>handleDisplay()}
          size="lg"
          color={'info'}
        >
          <CModalHeader closeButton style={{textAlign: 'center'}}>Enregistrement Site</CModalHeader>
          <CModalBody>
            <CForm>
              <CFormGroup>
                
                  <CCol>
                    <CInput id="adresse" name="adresse" placeholder="Adresse" value={formik.values.adresse} onChange={formik.handleChange} />
                    <p className="text-warning" >{formik.errors.adresse?formik.errors.adresse:null}</p>
                  </CCol>

                  <CCol>
                    <CInput id="ville" name="ville" placeholder="Ville" value={formik.values.ville} onChange={formik.handleChange} />
                    <p className="text-warning" >{formik.errors.ville?formik.errors.ville:null}</p>
                  </CCol>

                  <CCol>
                    <CInput id="zipCode" name="zipCode" placeholder="Code Postal" value={formik.values.zipCode} onChange={formik.handleChange} />
                    <p className="text-warning" >{formik.errors.zipCode?formik.errors.zipCode:null}</p>
                  </CCol>

                  <CCol>
                    <CInput id="tel" name="tel" placeholder="Numéro de téléphone" value={formik.values.tel} onChange={formik.handleChange} />
                    <p className="text-warning" >{formik.errors.tel?formik.errors.tel:null}</p>
                  </CCol>

                  <CCol>
                    <CInput id="email" name="email" placeholder="E-mail" value={formik.values.email} onChange={formik.handleChange} />
                    <p className="text-warning" >{formik.errors.email?formik.errors.email:null}</p>
                  </CCol>
                  <CInput type='hidden' id="id" name="id" placeholder="id" value={formik.values.id} onChange={formik.handleChange} />

                <CFormText className="help-block" color={'danger'}>{mess}</CFormText>
              </CFormGroup>
            </CForm>
          </CModalBody>
          <CModalFooter>
            <CButton onClick={formik.handleSubmit} type="submit" color="info">Enregistrement des données</CButton>{' '}
            <CButton
              color="secondary"
              onClick={(e) => handleDisplay()}
            >Fermer</CButton>
          </CModalFooter>
        </CModal>
      </>
    )
  })

export default withSwal (AddSite)
