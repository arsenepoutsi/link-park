import React, { useEffect, useState } from 'react';
import {useHistory} from 'react-router-dom'
import { useFormik } from 'formik';
import axios from '../services/api'
import { withSwal } from 'react-sweetalert2';
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CRow, CCol,
  CForm,
  CFormGroup,
  CInput,
  CFormText,
  CSelect,
  CLabel
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
//import { changeUserInfo } from './Users';


const AjoutCar = ({swal}) =>{
  const [status, setStatus] = useState(0)
  const [carData, setCarData] = useState([])
  const [showModal, setShowModal] = useState(false)
  const [rowID, setRowID] =useState(null)
  const [rowData, setRowData] = useState([])
  const [loadId, setLoadId] = useState(1)
  const fields = [
    'picture',
    'immat', 
    'marque', 
    'model', 
    'volumeCoffre', 
    'isManuelle', 
    'nbPlace', 
    'state',  
    'action'
  ]

  const hanldeShowModal = () => {
    setRowID(null)
    setShowModal(!showModal)
  }

  const addOrEdit = (id, item) => {
    setRowID(id)
    setRowData(item)
  }

  useEffect(() => {
    if (rowID !== null){
      setShowModal(!showModal)
    }
  }, [rowID])

  function handleAddNew() {
    setStatus(status + 1)
  }

 const deletePost = (immat) => {
  swal.fire({
    title: 'Êtes-vous sûr ?',
    text: "Vous ne pourrez pas revenir en arrière",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#3085d6',
    confirmButtonText: 'Oui, Supprimer'
  }).then((result) => {
    if (result.isConfirmed) {
      axios.delete(`/car/DeleteByImmat/${immat}`)
      .then(() => setLoadId(loadId+1))
      swal.fire(
        'Suppression',
        'Suppression réussie.',
        'success'
      )
    }
  })
};

 useEffect(() => {
  axios.get('/Car/GetAll', {
    responseType: 'json'
  }).then(response => {
    setCarData(response.data.map(car => {
      return {
        ...car
      }
    }))
  })
 }, [loadId])

  return (
    <>
      <CRow className="justify-content-center">
        <CCol md="12">
          <CCard>
            {fields.includes('action')&&(
              <CCardHeader >
                <CRow>
                  <CCol>
                    <CButton onClick={()=>addOrEdit(-1, [])} className="px-5" color="info">+ Ajouter un Véhicule</CButton>
                    <Modal rowID={rowID}  display={showModal} rowData={rowData} handleDisplay={hanldeShowModal}  handleAddNew={handleAddNew} refresh={() => setLoadId(loadId+1)}/>
                  </CCol>
                </CRow>
              </CCardHeader>
            )}
            <CCardBody>
              <CDataTable
                items={carData}
                fields={fields}
                itemsPerPageSelect
                itemsPerPage={5}
                pagination
                tableFilter
                scopedSlots = {{
                  'picture' : (item) => (
                    <td>
                      <div className={'c-avatar'}>
                        <img className={'c-avatar-img'} src={`http://163.172.151.186/${item.picture}`} alt={''}/>
                      </div>
                    </td>
                  ),
                  'immat': (item) => (
                    <td>
                      <b>{item.immat}</b>
                    </td>
                  ),
                  'marque':(item)=>(
                    <td>
                        {item.marque}
                    </td>
                  ),
                  'model':(item) => (
                    <td>
                        {item.model}
                    </td>
                  ),
                  'nbPlace':(item) => (
                    <td>
                        {item.nbPlace} Places
                    </td>
                  ),
                  'volumeCoffre':(item) => (
                    <td>
                        {item.volumeCoffre} Litres
                    </td>
                  ),
                  'isManuelle':
                    (item)=>(
                      <td>
                          <CBadge shape={'pill'} color={item.isManuelle == false? 'info' : 'primary'}>
                            {item.isManuelle == false? "Automatique" : "Manuelle"}
                          </CBadge>
                      </td>
                    ),
                    'state':
                    (item)=>(
                      <td>
                          <CBadge shape={'pill'} color={item.state == 'AVAILABLE'? 'warning' : 'danger'}>
                            {item.state == 'AVAILABLE'? "Disponible" : "Indisponible"}
                          </CBadge>
                      </td>
                    ),
                  'action': (item) => (
                    <td width={102}>
                      <CRow>
                        <CCol>
                            <CButton onClick={(e)=> addOrEdit(item.id, item)} className={'btn-pill'} size={'sm'} ><CIcon className={'cust_action_edit'} name={'cilPencil'} /></CButton>
                        </CCol>
                        <CCol>
                            <CButton onClick={(e) => deletePost(item.immat)}  className={'btn-pill'} size={'sm'} ><CIcon className={'cust_action_delete'} name={'cilTrash'}/></CButton>
                        </CCol>
                      </CRow>
                    </td>
                  )
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

const Modal = withSwal ((props) => {
    const {swal} = props
    const [mess, setMess] = useState('')
    const history = useHistory()
    const rowData = props.rowData
    const [sites, setSiteData] = useState([])

     //Affichage Site
     useEffect(() => {
      axios.get('/Site/getAll')
      .then(res => {
        const reponses = res.data;
        setSiteData(reponses);
      })
      }, [])

    function handleAddNewOne() {
      props.handleAddNew();
    }
    const handleDisplay = () => {
      setMess('')
      props.handleDisplay()
    }
  
    const validate = values => {
      const errors = {};
      values.immat || (errors.immat = 'Champ obligatoire');
      values.marque || (errors.marque = 'Champ obligatoire');
      values.model || (errors.model = 'Champ obligatoire');
      values.volumeCoffre || (errors.volumeCoffre = 'Champ obligatoire');
      values.isUtilitaire || (errors.isUtilitaire = 'Champ obligatoire');
      values.isManuelle || (errors.isManuelle = 'Champ obligatoire');
      values.nbPlace || (errors.nbPlace = 'Champ obligatoire');
      values.state || (errors.state = 'Champ obligatoire');
      return errors;
    }
    const formik = useFormik({
      enableReinitialize: true,
      initialValues: {
        id: rowData.id,
        immat: rowData.immat,
        marque: rowData.marque || '',
        model: rowData.model || '',
        volumeCoffre: rowData.volumeCoffre || '',
        isUtilitaire: rowData.isUtilitaire ?'m':'a',
        isManuelle: rowData.isManuelle ?'m':'a',
        nbPlace: rowData.nbPlace || '',
        state: rowData.state || '',
        siteId: rowData.siteId || '',
        picture: rowData.picture || '',
      },
      validate,
      onSubmit: values => {
        add_car(values)
      }
    })

    async function add_car(values) {
        console.log(values.id);
        const access_token = (localStorage.getItem('access_token'))
        if (access_token){
          try {
            if (!values.id){
                axios
                .post(`/Car/AddOrUpdate/`, {
                    immat: values.immat,
                    marque: values.marque, 
                    model: values.model, 
                    volumeCoffre: values.volumeCoffre, 
                    isUtilitaire: values.isUtilitaire=='m',
                    isManuelle: values.isManuelle=='m',
                    nbPlace: values.nbPlace,
                    state: values.state,
                    siteId: values.siteId,
                    picture: values.picture
                })
                .then(response => {
                  handleAddNewOne();
                  handleDisplay();
                  // history.push('/add-car')
                  props.refresh()
                  swal.fire({
                      title: 'Succès',
                      text: 'Enregistrement avec succès',
                      icon: 'success',
                  });
                }).catch(err => {
                  setMess("Erreur d'enregistrement !")
                });
            }else{
                axios
                .post(`/Car/AddOrUpdate/`, {
                    id: values.id,
                    immat: values.immat,
                    marque: values.marque, 
                    model: values.model, 
                    volumeCoffre: values.volumeCoffre, 
                    isUtilitaire: values.isUtilitaire=='m',
                    isManuelle: values.isManuelle=='m',
                    nbPlace: values.nbPlace,
                    state: values.state,
                    siteId: values.siteId,
                    picture: values.picture
                })
                .then(response => {
                  handleAddNewOne();
                  handleDisplay();
                  // history.push('/add-car')
                  props.refresh()
                  swal.fire({
                      title: 'Succès',
                      text: 'Modification avec succès',
                      icon: 'success',
                  });
                }).catch(err => {
                  setMess("Erreur d'enregistrement !")
                });
            }
          } catch (e) {
            alert(e.message)
            history.push('/login')
            localStorage.removeItem('access_token')
          }
        }
    
      }

    return (
      <>
        <CModal
          show={props.display}
          onClose={(e)=>handleDisplay()}
          size="lg"
          color={'info'}
        >
          <CModalHeader closeButton style={{textAlign: 'center'}}>Enregistrement Véhicule</CModalHeader>
          <CModalBody>
            <CForm>
              <CFormGroup>

                  <CRow>
                    <CCol md={6}>
                        <CLabel htmlFor="exampleInputPassword1">Immatriculation</CLabel>
                        <CInput id="immat" name="immat" value={formik.values.immat} onChange={formik.handleChange} />
                        <p className="text-warning" >{formik.errors.immat?formik.errors.immat:null}</p>
                    </CCol>
                    <CCol md={6}>
                        <CLabel htmlFor="exampleInputPassword1">Marque</CLabel>
                        <CInput id="marque" name="marque" value={formik.values.marque} onChange={formik.handleChange} />
                        <p className="text-warning" >{formik.errors.marque?formik.errors.marque:null}</p>
                    </CCol>
                  </CRow>

                  <CRow>
                    <CCol md={6}>
                        <CLabel htmlFor="exampleInputPassword1">Modèle</CLabel>
                        <CInput id="model" name="model" value={formik.values.model} onChange={formik.handleChange} />
                        <p className="text-warning" >{formik.errors.model?formik.errors.model:null}</p>
                    </CCol>
                    <CCol md={6}>
                        <CLabel htmlFor="exampleInputPassword1">Volume du Coffre (En litre)</CLabel>
                        <CInput id="volumeCoffre" name="volumeCoffre" value={formik.values.volumeCoffre} onChange={formik.handleChange} />
                        <p className="text-warning" >{formik.errors.volumeCoffre?formik.errors.volumeCoffre:null}</p>
                    </CCol>
                  </CRow>

                  <CRow>
                    <CCol md={6}>
                        <CLabel htmlFor="exampleInputPassword1">Nombre de place</CLabel>
                        <CInput id="nbPlace" name="nbPlace" value={formik.values.nbPlace} onChange={formik.handleChange} />
                        <p className="text-warning" >{formik.errors.nbPlace?formik.errors.nbPlace:null}</p>
                    </CCol>
                    <CCol md={6}>
                        <CLabel htmlFor="exampleInputPassword1">Site</CLabel>
                        <CSelect aria-label="siteId" id="siteId" name="siteId" value={formik.values.siteId} onChange={formik.handleChange}                  
                          >
                            {sites && sites.map(site => <option value={site.id} key={site.id.toString()}>{site.ville}</option>)}
                        </CSelect>
                    </CCol>
                  </CRow>

                  <CRow>
                    <CCol md={4}>
                        <CLabel htmlFor="exampleInputPassword1">Utilitaire</CLabel>
                        <CSelect aria-label="isUtilitaire" id="isUtilitaire" value={formik.values.isUtilitaire} onChange={formik.handleChange}                  
                          >
                            <option>Choix de l'Utilitaire</option>
                            <option value="a">NON</option>
                            <option value="m">OUI</option>
                        </CSelect>
                        <p className="text-warning" >{formik.errors.isUtilitaire?formik.errors.isUtilitaire:null}</p>
                    </CCol>
                    <CCol md={4}>
                        <CLabel htmlFor="exampleInputPassword1">Boîte de vitesse</CLabel>
                        <CSelect aria-label="isManuelle" id="isManuelle" value={formik.values.isManuelle} onChange={formik.handleChange}                  
                          >
                            <option>Choix de la Boîte de vitesse</option> 
                            <option value="a">Boîte Automatique</option>
                            <option value="m">Boîte Manuelle</option>
                        </CSelect>
                        <p className="text-warning" >{formik.errors.isManuelle?formik.errors.isManuelle:null}</p>
                    </CCol>
                    <CCol md={4}>
                        <CLabel htmlFor="exampleInputPassword1">Statut du Véhicule</CLabel>
                        <CSelect aria-label="state" id="state" name="state" value={formik.values.state} onChange={formik.handleChange}                  
                          >
                            <option>Choisir le Statut</option>
                            <option value="AVAILABLE">DISPONIBLE</option>
                            <option value="UNAVAILABLE">INDISPONIBLE</option>
                        </CSelect>
                        <p className="text-warning" >{formik.errors.state?formik.errors.state:null}</p>
                    </CCol>
                  </CRow>
                
                  <CInput type='hidden' id="id" name="id" value={formik.values.id} onChange={formik.handleChange} />
                  <CInput type='hidden' id="picture" name="picture" value="/data/images/cars/BMW-I3.jpg" onChange={formik.handleChange} />

                <CFormText className="help-block" color={'danger'}>{mess}</CFormText>
              </CFormGroup>
            </CForm>
          </CModalBody>
          <CModalFooter>
            <CButton onClick={formik.handleSubmit} type="submit" color="info">Enregistrement des données</CButton>{' '}
            <CButton
              color="secondary"
              onClick={(e) => handleDisplay()}
            >Fermer</CButton>
          </CModalFooter>
        </CModal>
      </>
    )
  })

export default withSwal (AjoutCar)
