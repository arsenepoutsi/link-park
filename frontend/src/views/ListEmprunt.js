import React, { useEffect, useState } from 'react'
import {useHistory} from 'react-router-dom'
import { useFormik } from 'formik'
import axios from '../services/api'
import { withSwal } from 'react-sweetalert2';
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CRow, CCol,
  CForm,
  CFormGroup,
  CInput,
  CFormText,
  CCollapse,
  CCallout,
  CLabel,
  CSelect
} from '@coreui/react'

const ListEmprunt = () => {
    const [showModal, setShowModal] = useState(false)
    const [rowID, setRowID] =useState(null)
    const [rowData, setRowData] = useState([])
    const [status, setStatus] = useState(0)
    const [details, setDetails] = useState([])
    const [empruntData, setEmpruntData] = useState([])
    const [loadId, setLoadId] = useState(1)

    const hanldeShowModal = () => {
      setRowID(null)
      setShowModal(!showModal)
    }
  
    const addOrEdit = (id, item) => {
      setRowID(id)
      setRowData(item)
    }
  
    useEffect(() => {
      if (rowID !== null){
        setShowModal(!showModal)
      }
    }, [rowID])
  
    function handleAddNew() {
      setStatus(status + 1)
    }

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }


  const fields = [
    'Nom',
    'Véhicule', 
    'Passagers',
    'Date début', 
    'Statut',
    {
      key: 'show_details',
      label: '',
      _style: { width: '1%' },
      sorter: true,
      filter: true
    }
  ]

  useEffect(() => {
    axios.get('/Reservation/GetAll', {
      responseType: 'json'
    }).then(async response => {
      const res = (await Promise.allSettled(
        response.data.map(async reservation=>{
          //Récupération du User
          reservation.user = (await axios.get('/User/GetUser/'+reservation.userId, {
            responseType: 'json'
          })).data
          //Récupétatuin Car
          reservation.car = (await axios.get('/Car/GetByImmat/'+reservation.immat, {
            responseType: 'json'
          })).data
          return reservation; 
        })
      )).filter(res=>res.status == "fulfilled").map(res=>res.value);
      setEmpruntData(res)
    })
   }, [loadId])

    return (
    <>
      <CRow className="justify-content-center">
        <CCol md="12">
          <CCard>
              <CCardHeader >
                <CRow className="justify-content-center">
                  <CCol xs={12}>
                    <CCallout color="info" className="bg-white">
                      <h1>Liste d'Emprunts</h1>
                    </CCallout>
                  </CCol>
                </CRow>
              </CCardHeader>
            <CCardBody>
            <CDataTable
              items={empruntData}
              fields={fields}
              columnFilter
              tableFilter
              footer
              itemsPerPageSelect
              itemsPerPage={10}
              hover
              sorter={{ column: 'Statut', state: 'desc' }}
              pagination
              scopedSlots = {{
                'Nom':(item) => (
                  <td>
                    <b>{item.user.firstName} {item.user.lastName}</b>
                  </td>
                ),
                'Véhicule' : (item) => (
                  <td>
                    <b>{item.car.marque} {item.car.model}</b>
                  </td>
                ),
                'Passagers' : (item) => (
                  <td>
                    {item.passengers.map(passenger => passenger.firstName +" " +passenger.lastName)}
                  </td>
                ),
                'Destination' :(item) => (
                  <td>
                     {item.destination.address} {item.destination.city}
                  </td>
                ),
                'Date début' :(item) => (
                  <td>
                     <b>{item.startDate}</b>
                  </td>
                ),
                'Statut':
                    (item)=>(
                      <td>
                          <CBadge shape={'pill'} color={item.state == 'ACCEPTED'? 'success' 
                            : item.state == 'CANCELED'? 'danger' 
                            : 'warning'}>
                            {item.state == 'ACCEPTED'? "Demande Acceptée" 
                              : item.state == 'CANCELED'? "Demande Annulée" 
                              : "En cours de Confirmation"}
                          </CBadge>
                      </td>
                    ),
                'show_details':
                  (item, index)=>{
                    return (
                      <td className="py-2">
                        <CButton
                          color="primary"
                          variant="outline"
                          shape="square"
                          size="sm"
                          onClick={()=>{toggleDetails(index)}}
                        >
                          {details.includes(index) ? 'Cacher' : 'Détails'}
                        </CButton>
                      </td>
                      )
                  },
                'details':
                    (item, index)=>{
                      return (
                      <CCollapse show={details.includes(index)}>
                        <CCardBody>
                          <h4>
                            Date fin : {item.endDate}
                          </h4>
                          <CButton onClick={(e)=> addOrEdit(item.id, item)} size="sm" color="success">
                            Confirmer/Annuler
                          </CButton>
                          <Modal rowID={rowID}  display={showModal} rowData={rowData} handleDisplay={hanldeShowModal}  handleAddNew={handleAddNew}  refresh={() => setLoadId(loadId+1)}/>
                        </CCardBody>
                      </CCollapse>
                    )
                  }
              }}
            />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

const Modal = withSwal ((props) => {
  const {swal} = props
  const [mess, setMess] = useState('')
  const history = useHistory()
  const rowData = props.rowData
  const handleAddNewOne = () => {
    props.handleAddNew();
  }
  const handleDisplay = () => {
    setMess('')
    props.handleDisplay()
  }

  const validate = values => {
    const errors = {};
    values.state || (errors.state = 'Obligatoire');
    return errors;
  }
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      state: '',
      reservationId: rowData.id,
    },
    validate,
    onSubmit: values => {
      add_reservation(values)
    }
  })

  async function add_reservation(values) {
      const access_token = (localStorage.getItem('access_token'))
      if (access_token){
        try {
          axios
          .post(`/Reservation/UpdateState?reservationId=${values.reservationId}&state=${values.state}`)
          .then(response => {
            handleAddNewOne();
            handleDisplay();
            // history.push('/demande-emprunt')
            props.refresh()
            swal.fire({
                title: 'Succès',
                text: 'Confirmation/Annulation avec succès',
                icon: 'success',
            });
          }).catch(err => {
            setMess("Erreur d'enregistrement !")
          });
        } catch (e) {
          alert(e.message)
          history.push('/login')
          localStorage.removeItem('access_token')
        }
      }
  
    }

  return (
    <>
      <CModal
        show={props.display}
        onClose={(e)=>handleDisplay()}
        size="lg"
        color={'info'}
      >
        <CModalHeader closeButton style={{textAlign: 'center'}}>Confirmation/Modification de la Demande d'Emprunt</CModalHeader>
        <CModalBody>
          <CForm>
            <CFormGroup>
              
                <CCol>
                  <CLabel htmlFor="exampleInputPassword1">Choisir le nouveau statut</CLabel>
                  <CSelect aria-label="state" id="state" name="state" value={formik.values.state} onChange={formik.handleChange}                  
                    >
                      <option>Choisir le Statut</option>
                      <option value="ACCEPTED">Accepter la Demande</option>
                      <option value="CANCELED">Annuler la Demande</option>
                  </CSelect>
                  <p className="text-warning" >{formik.errors.state?formik.errors.state:null}</p>
                </CCol>

                  <CInput 
                    id="reservationId" 
                    type='hidden' 
                    name="reservationId" 
                    label="reservation Id" 
                    value={formik.values.reservationId} 
                    onChange={formik.handleChange} 
                  />

              <CFormText className="help-block" color={'danger'}>{mess}</CFormText>
            </CFormGroup>
          </CForm>
        </CModalBody>
        <CModalFooter>
          <CButton onClick={formik.handleSubmit} type="submit" color="info">Confirmer/Annuler</CButton>{' '}
          <CButton
            color="secondary"
            onClick={(e) => handleDisplay()}
          >Fermer</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
})

export default ListEmprunt
