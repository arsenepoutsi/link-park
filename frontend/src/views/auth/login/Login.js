import React, { useEffect, useState, useCallback } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { withSwal } from 'react-sweetalert2'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import axios from '../../../services/api'
import { useFormik } from 'formik';

const Login = withSwal (({swal}) => {
  const [message, setMessage] = useState("")
  const history = useHistory()

  const validate = values => {
    const errors = {};
    if (!values.password) {
      errors.password = 'Obligatoire';
    }

    if (!values.email) {
      errors.email = 'Obligatoire';
    }
    return errors;
  };

  const formik = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    validate,
    onSubmit: values => {
      user_login(values)
    }
  })

  const user_login = useCallback( async function (values) {
    try {
        await axios.login({ email: values.email, password: values.password }).then(response => {
        // history.push('/')
        window.location.href="/"
        //window.location.href = '/'
      // }).catch(err => {
      //   console.log(err)
      //   setMessage(err)
      })
    } catch(e){
      swal.fire({
          title: 'Erreur',
          text: 'Mot de passe ou E-mail incorrect',
          icon: 'error',
      });
    }
  }, [])


  useEffect(() => {
    if (localStorage.getItem('access_token')){
      history.push('/dashboard')
      //window.location.href = '/dashboard'
    }
  },[])

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">

          <CCol xs={5}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={formik.handleSubmit}>
                    <h1 style={{ textAlign: 'center' }}>Authentification</h1>
                    <p className="text-muted" style={{ textAlign: 'center' }}>Connectez-vous à votre compte</p>
                    <p className="text-danger">{message}</p>
                    <p className="text-warning field_validate_label" >{formik.errors.email?formik.errors.email:null}</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>

                      <CInput id="email" name="email" type="email" placeholder="E-mail" autoComplete="username" value={formik.values.email} onChange={formik.handleChange}  />
                    </CInputGroup>

                    <p className="text-warning field_validate_label" >{formik.errors.password?formik.errors.password:null}</p>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput id="password" name="password" type="password" placeholder="Mot de passe" autoComplete="current-password" value={formik.values.password} onChange={formik.handleChange}  />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton type="submit" color="primary" className="px-4">Se connecter</CButton>
                      </CCol>
                      {/* <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">Mot de passe oublié ?</CButton>
                      </CCol> */}
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>

          <CCol xs={4} 
            style={{
              backgroundImage: `url("http://163.172.151.186/data/images/society/ENI_Ecole/Logo_ENI.png")`, 
              backgroundSize: 'cover', 
              overflow: 'hidden', 
              backgroundRepeat  : 'no-repeat', 
              backgroundPosition: 'center', 
              marginLeft: -15
            }}>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
})

export default Login
