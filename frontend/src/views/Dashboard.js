import React, { useEffect, useState, lazy } from 'react'
import {useHistory} from 'react-router-dom'
import { useFormik } from 'formik'
import axios, {getUser} from '../services/api'
import { withSwal } from 'react-sweetalert2';
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CRow, CCol,
  CForm,
  CFormGroup,
  CInput,
  CFormText,
  CCollapse,
  CCallout,
  CLabel
} from '@coreui/react'


const WidgetsDropdown = lazy(() => import('./widgets/WidgetsDropdown'))

const Dashboard = () => {
    const [showModal, setShowModal] = useState(false)
    const [rowID, setRowID] =useState(null)
    const [rowData, setRowData] = useState([])
    const [status, setStatus] = useState(0)
    const [loadId, setLoadId] = useState(1)

    const hanldeShowModal = () => {
      setRowID(null)
      setShowModal(!showModal)
    }
  
    const addOrEdit = (id, item) => {
      setRowID(id)
      setRowData(item)
    }
  
    useEffect(() => {
      if (rowID !== null){
        setShowModal(!showModal)
      }
    }, [rowID])
  
    function handleAddNew() {
      setStatus(status + 1)
    }

    const validate = values => {
      const errors = {};
      if (!values.startDate) {
        errors.startDate = 'Obligatoire';
      }

      if (!values.endDate) {
        errors.endDate = 'Obligatoire';
      }
      return errors;
    };

    const formik = useFormik({
      initialValues: {
        startDate: '',
        endDate: ''
      },
      validate,
      onSubmit: values => {
        disponibility(values)
      }
    })

    async function disponibility(values) {
      axios.get(`/Car/GetAllAvailableForReservation?startDate=${values.startDate}&endDate=${values.endDate}`)
      .then(async response => {
        localStorage.setItem('date_debut', values.startDate)
        localStorage.setItem('date_fin', values.endDate)

        const res = (await Promise.allSettled(
          response.data.map(async disponibilite=>{
            //Récupération du User
            disponibilite.site = (await axios.get('/Site/GetById/'+disponibilite.siteId, {
              responseType: 'json'
            })).data
            return disponibilite; 
          })
        )).filter(res=>res.status == "fulfilled").map(res=>res.value);
        setCarData(res)
      })
     }

    //Données Tableau
    const [details, setDetails] = useState([])
    const [carData, setCarData] = useState([])
  // const [items, setItems] = useState(usersData)

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }


  const fields = [
    'picture',
    'immat', 
    'marque', 
    'model', 
    'isManuelle',
    'Site',
    {
      key: 'show_details',
      label: '',
      _style: { width: '1%' },
      sorter: false,
      filter: false
    }
  ]

  if(getUser().UserRole == 'Admin'){
    return (
      <>
          <WidgetsDropdown />
          <CRow className="justify-content-center">
            <CCol md="12">
              <CCard>
                  <CCardHeader >
                    <CRow className="justify-content-center">
                      <CCol xs={12}>
                        <CCallout color="info" className="bg-white">
                          <h1>Demande d'emprunt</h1>
                        </CCallout>
                      </CCol>
                    </CRow>
                  </CCardHeader>
                <CCardBody>
                <CForm>
                    <CRow>
                      <CCol md={3}></CCol>
                      <CCol md={3}>
                          <CLabel htmlFor="exampleInputPassword1">Date de début</CLabel>
                          <CInput
                            type='datetime-local' 
                            id="startDate" 
                            name="startDate" 
                            label="Date de fin"
                            value={formik.values.startDate} 
                            onChange={formik.handleChange}
                          />
                      </CCol>
                      <CCol md={3}>
                          <CLabel htmlFor="exampleInputPassword1">Date de fin</CLabel>
                          <CInput
                            type='datetime-local' 
                            id="endDate" 
                            name="endDate" 
                            label="Date de fin"
                            value={formik.values.endDate} 
                            onChange={formik.handleChange}
                          />
                      </CCol>
                      <CCol md={3}></CCol>
                    </CRow>

                    <CRow style={{marginTop:20, alignItems:'center', justifyContent:'center'}}>
                      <CCol md={4}></CCol>
                      <CCol md={4} style={{marginTop:20, alignItems:'center', justifyContent:'center'}}>
                        <CButton onClick={formik.handleSubmit} type="submit" color="info" style={{alignItems:'center', justifyContent:'center'}}>
                          Afficher les véhicules disponibles
                        </CButton>
                      </CCol>
                      <CCol md={4}></CCol>
                    </CRow>

                  </CForm>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
          {carData == '' ? '' :<CRow className="justify-content-center">
            <CCol md="12">
              <CCard>
                  <CCardHeader >
                    <CRow className="justify-content-center">
                      <CCol xs={12}>
                        <CCallout color="info" className="bg-white">
                          <h1>Véhicules Disponibles</h1>
                        </CCallout>
                      </CCol>
                    </CRow>
                  </CCardHeader>
                <CCardBody>
                <CDataTable
                  items={carData}
                  fields={fields}
                  columnFilter
                  tableFilter
                  footer
                  itemsPerPageSelect
                  itemsPerPage={5}
                  hover
                  sorter
                  pagination
                  scopedSlots = {{
                    'picture' : (item) => (
                      <td>
                        <div className={'c-avatar'}>
                          <img className={'c-avatar-img'} src={`http://163.172.151.186/${item.picture}`} alt={''}/>
                        </div>
                      </td>
                    ),
                    'immat': (item) => (
                      <td>
                        <b>{item.immat}</b>
                      </td>
                    ),
                    'marque':(item)=>(
                      <td>
                          {item.marque}
                      </td>
                    ),
                    'model':(item) => (
                      <td>
                          {item.model}
                      </td>
                    ),
                    'isManuelle':
                      (item)=>(
                        <td>
                            <CBadge shape={'pill'} color={item.isManuelle == false? 'danger' : 'warning'}>
                              {item.isManuelle == false? "Automatique" : "Manuelle"}
                            </CBadge>
                        </td>
                      ),
                    'Site':(item) => (
                      <td>
                        <b>{item.site.ville}</b>
                      </td>
                    ),
                    'show_details':
                      (item, index)=>{
                        return (
                          <td className="py-2">
                            <CButton
                              color="primary"
                              variant="outline"
                              shape="square"
                              size="sm"
                              onClick={()=>{toggleDetails(index)}}
                            >
                              {details.includes(index) ? 'Cacher' : 'Détails'}
                            </CButton>
                          </td>
                          )
                      },
                    'details':
                        (item, index)=>{
                          return (
                          <CCollapse show={details.includes(index)}>
                            <CCardBody>
                              <h4>
                                Nombre de place : {item.nbPlace}
                              </h4>
                              <p className="text-muted">Volume du coffre : {item.volumeCoffre} Litres</p>
                              <CButton onClick={(e)=> addOrEdit(item.id, item)} size="sm" color="warning">
                                Réserver
                              </CButton>
                              <Modal rowID={rowID}  display={showModal} rowData={rowData} handleDisplay={hanldeShowModal}  handleAddNew={handleAddNew} refresh={() => setLoadId(loadId+1)}/>
                            </CCardBody>
                          </CCollapse>
                        )
                      }
                  }}
                />
                </CCardBody>
              </CCard>
            </CCol>
          </CRow> }
      </>
    )
  }else{
    return (
      <>
          <CRow className="justify-content-center">
            <CCol md="12">
              <CCard>
                  <CCardHeader >
                    <CRow className="justify-content-center">
                      <CCol xs={12}>
                        <CCallout color="info" className="bg-white">
                          <h1>Demande d'emprunt</h1>
                        </CCallout>
                      </CCol>
                    </CRow>
                  </CCardHeader>
                <CCardBody>
                <CForm>
                    <CRow>
                      <CCol md={3}></CCol>
                      <CCol md={3}>
                          <CLabel htmlFor="exampleInputPassword1">Date de début</CLabel>
                          <CInput
                            type='datetime-local' 
                            id="startDate" 
                            name="startDate" 
                            label="Date de fin"
                            value={formik.values.startDate} 
                            onChange={formik.handleChange}
                          />
                      </CCol>
                      <CCol md={3}>
                          <CLabel htmlFor="exampleInputPassword1">Date de fin</CLabel>
                          <CInput
                            type='datetime-local' 
                            id="endDate" 
                            name="endDate" 
                            label="Date de fin"
                            value={formik.values.endDate} 
                            onChange={formik.handleChange}
                          />
                      </CCol>
                      <CCol md={3}></CCol>
                    </CRow>

                    <CRow style={{marginTop:20, alignItems:'center', justifyContent:'center'}}>
                      <CCol md={4}></CCol>
                      <CCol md={4} style={{marginTop:20, alignItems:'center', justifyContent:'center'}}>
                        <CButton onClick={formik.handleSubmit} type="submit" color="info" style={{alignItems:'center', justifyContent:'center'}}>
                          Afficher les véhicules disponibles
                        </CButton>
                      </CCol>
                      <CCol md={4}></CCol>
                    </CRow>

                  </CForm>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
          {carData == '' ? '' :<CRow className="justify-content-center">
            <CCol md="12">
              <CCard>
                  <CCardHeader >
                    <CRow className="justify-content-center">
                      <CCol xs={12}>
                        <CCallout color="info" className="bg-white">
                          <h1>Véhicules Disponibles</h1>
                        </CCallout>
                      </CCol>
                    </CRow>
                  </CCardHeader>
                <CCardBody>
                <CDataTable
                  items={carData}
                  fields={fields}
                  columnFilter
                  tableFilter
                  footer
                  itemsPerPageSelect
                  itemsPerPage={5}
                  hover
                  sorter
                  pagination
                  scopedSlots = {{
                    'picture' : (item) => (
                      <td>
                        <div className={'c-avatar'}>
                          <img className={'c-avatar-img'} src={`http://163.172.151.186/${item.picture}`} alt={''}/>
                        </div>
                      </td>
                    ),
                    'immat': (item) => (
                      <td>
                        <b>{item.immat}</b>
                      </td>
                    ),
                    'marque':(item)=>(
                      <td>
                          {item.marque}
                      </td>
                    ),
                    'model':(item) => (
                      <td>
                          {item.model}
                      </td>
                    ),
                    'isManuelle':
                      (item)=>(
                        <td>
                            <CBadge shape={'pill'} color={item.isManuelle == false? 'danger' : 'warning'}>
                              {item.isManuelle == false? "Automatique" : "Manuelle"}
                            </CBadge>
                        </td>
                      ),
                      'Site':(item) => (
                        <td>
                          <b>{item.site.ville}</b>
                        </td>
                      ),
                    'show_details':
                      (item, index)=>{
                        return (
                          <td className="py-2">
                            <CButton
                              color="primary"
                              variant="outline"
                              shape="square"
                              size="sm"
                              onClick={()=>{toggleDetails(index)}}
                            >
                              {details.includes(index) ? 'Cacher' : 'Détails'}
                            </CButton>
                          </td>
                          )
                      },
                    'details':
                        (item, index)=>{
                          return (
                          <CCollapse show={details.includes(index)}>
                            <CCardBody>
                              <h4>
                                Nombre de place : {item.nbPlace}
                              </h4>
                              <p className="text-muted">Volume du coffre : {item.volumeCoffre} Litres</p>
                              <CButton onClick={(e)=> addOrEdit(item.id, item)} size="sm" color="warning">
                                Réserver
                              </CButton>
                              <Modal rowID={rowID}  display={showModal} rowData={rowData} handleDisplay={hanldeShowModal}  handleAddNew={handleAddNew} refresh={() => setLoadId(loadId+1)}/>
                            </CCardBody>
                          </CCollapse>
                        )
                      }
                  }}
                />
                </CCardBody>
              </CCard>
            </CCol>
          </CRow> }
          
      </>
    )
  }
}

const Modal = withSwal ((props) => {
  const {swal} = props
  const [mess, setMess] = useState('')
  const history = useHistory()
  const rowData = props.rowData

  const Start_Date = localStorage.getItem('date_debut');
  const End_Date = localStorage.getItem('date_fin');

  const handleAddNewOne = () => {
    props.handleAddNew();
  }
  const handleDisplay = () => {
    setMess('')
    props.handleDisplay()
  }

  const validate = values => {
    const errors = {};
    values.city || (errors.city = 'Required');
    values.address || (errors.address = 'Required');
    return errors;
  }
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      startDate: Start_Date,
      endDate: End_Date,
      carId: rowData.id,
      userId: getUser().UserId,
      city: '',
      address: '',
    },
    validate,
    onSubmit: values => {
      add_reservation(values)
    }
  })

  async function add_reservation(values) {
      const access_token = (localStorage.getItem('access_token'))
      if (access_token){
        try {
          axios
          .post(`/Reservation/RequestReservation/`, {
              startDate: values.startDate,
              endDate: values.endDate, 
              carId: values.carId, 
              userId: values.userId, 
              destination: {
                city: values.city,
                address: values.address
              },
              passengers: [
                  {
                  firstName: values.firstName,
                  lastName: values.lastName
                }
              ]  
          })
          .then(response => {
            handleAddNewOne();
            handleDisplay();
            // history.push('/demande-emprunt')
            props.refresh()
            swal.fire({
                title: 'Succès',
                text: 'Réservation avec succès. Un mail de confirmation vient de vous être envoyé',
                icon: 'success',
            });
          }).catch(err => {
            setMess("Erreur d'enregistrement !")
          });
        } catch (e) {
          alert(e.message)
          history.push('/login')
          localStorage.removeItem('access_token')
        }
      }
  
    }

  return (
    <>
      <CModal
        show={props.display}
        onClose={(e)=>handleDisplay()}
        size="lg"
        color={'info'}
      >
        <CModalHeader closeButton style={{textAlign: 'center'}}>Nouvelle Réservation</CModalHeader>
        <CModalBody>
          <CForm>
            <CFormGroup>
              
                  <CInput 
                    type='hidden'
                    id="startDate" 
                    name="startDate" 
                    label="Date de début"
                    value={formik.values.startDate} onChange={formik.handleChange}
                  />

                  <CInput
                    type='hidden' 
                    id="endDate" 
                    name="endDate" 
                    label="Date de fin"
                    value={formik.values.endDate} onChange={formik.handleChange}
                  />

                  <CInput 
                    id="carId" 
                    type='hidden' 
                    name="carId" 
                    label="ID du véhicule" 
                    value={formik.values.carId} 
                    onChange={formik.handleChange} 
                  />

                <CCol>
                  <CLabel htmlFor="exampleInputPassword1">Ville de Destination</CLabel>
                  <CInput
                    id="city" 
                    name="city" 
                    label="Ville de Destination" 
                    value={formik.values.city} 
                    onChange={formik.handleChange}
                  />
                  <p className="text-warning" >{formik.errors.city?formik.errors.city:null}</p>
                </CCol>

                <CCol>
                  <CLabel htmlFor="exampleInputPassword1">Adresse de Destination</CLabel>
                  <CInput 
                    id="address" 
                    name="address" 
                    label="Adresse de Destination" 
                    value={formik.values.address} 
                    onChange={formik.handleChange} 
                  />
                  <p className="text-warning" >{formik.errors.address?formik.errors.address:null}</p>
                </CCol>

                  <CInput 
                    id="userId" 
                    type='hidden'
                    name="userId"  
                    label="ID Utilisateur" 
                    value={formik.values.userId} 
                    onChange={formik.handleChange} 
                  />

                <CCol>
                  <CLabel htmlFor="exampleInputPassword1">Prénom du passager</CLabel>
                  <CInput 
                    id="firstName" 
                    name="firstName" 
                    label="Prénom du passager"
                    value={formik.values.firstName} 
                    onChange={formik.handleChange} 
                  />
                  <p className="text-warning" >{formik.errors.firstName?formik.errors.firstName:null}</p>
                </CCol>

                <CCol>
                  <CLabel htmlFor="exampleInputPassword1">Nom du passager</CLabel>
                  <CInput 
                    id="lastName" 
                    name="lastName" 
                    placeholder="Nom du passager" 
                    value={formik.values.lastName} 
                    onChange={formik.handleChange} 
                  />
                  <p className="text-warning" >{formik.errors.lastName?formik.errors.lastName:null}</p>
                </CCol>

              <CFormText className="help-block" color={'danger'}>{mess}</CFormText>
            </CFormGroup>
          </CForm>
        </CModalBody>
        <CModalFooter>
          <CButton onClick={formik.handleSubmit} type="submit" color="info">Envoyer la Réservation</CButton>{' '}
          <CButton
            color="secondary"
            onClick={(e) => handleDisplay()}
          >Fermer</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
})

export default Dashboard
