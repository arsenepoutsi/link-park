import React, { useEffect, useState } from 'react';
import axios, {getUser} from '../services/api'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CRow, CCol,
  CCallout
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
//import { changeUserInfo } from './Users';


const Historiques = () =>{
    const [historiqueData, setHistoriqueData] = useState([])
    const [loadId, setLoadId] = useState(1)

  const fields = [
    'Véhicule', 
    'Passagers', 
    'Destination', 
    'Date début', 
    'Date fin', 
    'Statut'
  ]

  useEffect(() => {
    const utilisateur = getUser().UserId;
    console.log(utilisateur);
    axios.get('/Reservation/GetReservationByUser?userId='+utilisateur, {
      responseType: 'json'
    }).then(async response => {
      const res = (await Promise.allSettled(
        response.data.map(async reservation=>{
          //Récupération du User
          reservation.user = (await axios.get('/User/GetUser/'+reservation.userId, {
            responseType: 'json'
          })).data
          //Récupétatuin Car
          reservation.car = (await axios.get('/Car/GetByImmat/'+reservation.immat, {
            responseType: 'json'
          })).data
          return reservation; 
        })
      )).filter(res=>res.status == "fulfilled").map(res=>res.value);
      setHistoriqueData(res)
    })
   }, [loadId])
  

  return (
    <>
      <CRow className="justify-content-center">
        <CCol md="12">
          <CCard>
              <CCardHeader >
                <CRow className="justify-content-center">
                  <CCol xs={12}>
                    <CCallout color="info" className="bg-white">
                      <h1>Mes Historiques</h1>
                    </CCallout>
                  </CCol>
                </CRow>
              </CCardHeader>
            <CCardBody>
            <CDataTable
              items={historiqueData}
              fields={fields}
              columnFilter
              tableFilter
              footer
              itemsPerPageSelect
              itemsPerPage={10}
              hover
              sorter
              pagination
              scopedSlots = {{
                'Véhicule' : (item) => (
                  <td>
                    <b>Immat. {item.car.immat}</b> {item.car.marque} {item.car.model}
                  </td>
                ),
                'Passagers' : (item) => (
                  <td>
                    {item.passengers.map(passenger => passenger.firstName +" " +passenger.lastName)}
                  </td>
                ),
                'Destination' :(item) => (
                  <td>
                     {item.destination.address} <b>{item.destination.city}</b>
                  
                  </td>
                ),
                'Date début' :(item) => (
                  <td>
                     <b>{item.startDate}</b>
                  </td>
                ),
                'Date fin' :(item) => (
                  <td>
                     <b>{item.endDate}</b>
                  </td>
                ),
                'Statut':
                    (item)=>(
                      <td>
                          <CBadge shape={'pill'} color={item.state == 'ACCEPTED'? 'success' 
                            : item.state == 'CANCELED'? 'danger' 
                            : 'warning'}>
                            {item.state == 'ACCEPTED'? "Demande Acceptée" 
                              : item.state == 'CANCELED'? "Demande Annulée" 
                              : "En cours de Confirmation"}
                          </CBadge>
                      </td>
                    )
              }}
            />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>

    </>
  );
}

export default Historiques
