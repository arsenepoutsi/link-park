import React from 'react'
import { useHistory } from 'react-router-dom'
import axios from '../services/api'

import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react';
import CIcon from '@coreui/icons-react'

const TheHeaderDropdown = () => {
  // const history = useHistory()
  // const access_token = localStorage.getItem('access_token')
  async function logout() {
    try {
      localStorage.removeItem('access_token')
      // history.push('/login')
      window.location.href="/login"
    } catch (e) {
      alert(e.message)
    }
  }

  return (
    <CDropdown
      inNav
      className="c-header-nav-items mr-5"
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar cil-vertical-align-center ">
          <label className="pr-2">{axios.getUser().UserFullName}</label>
          <label><CIcon size={'xl'} name={'cilUser'} /></label>
          {/*<CImg*/}
          {/*  src={'avatars/6.jpg'}*/}
          {/*  className="c-avatar-img"*/}
          {/*  alt="admin@bootstrapmaster.com"*/}
          {/*/>*/}
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        {/*<CDropdownItem divider />*/}
        <CDropdownItem onClick={logout}>
          <CIcon name="cil-lock-locked" className="mfe-2"/>
          Déconnexion
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdown
