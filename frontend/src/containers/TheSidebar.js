import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import logo from '../assets/LogoLinkin.png'

// sidebar nav config
import navList from './_nav'

const TheSidebar = () => {
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebarShow)
  const access_token = localStorage.getItem('access_token')


  const navigation = navList.filter(item => {
    const res = item.role
    if (access_token) {
      if (res){
        const userRoles = access_token.user.roles.map((item) => {return item.nome})
        for (let i = 0; i < res.length; i++){
          if (userRoles.includes(res[i])) {
            return  true
          }
        }
        return false
      } else return true
    } else return false

  })


  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch({type: 'set', sidebarShow: val })}
    >
      <CSidebarBrand className="d-none d-md-flex" to="/">
        <img src={logo} style={{height: '35px', width: '230px'}}/>
        <CIcon className="sidebar-brand-narrow" height={35} />
      </CSidebarBrand>
      <CSidebarNav>

        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none"/>
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
