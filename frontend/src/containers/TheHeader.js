import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {getUser} from '../services/api'
import {
  CHeader,
  CToggler,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  CSubheader,
  CBreadcrumbRouter,
  CTooltip
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

// routes config
import routes from '../routes'

import {
  TheHeaderDropdown,
}  from './index'

const TheHeader = () => {
  const dispatch = useDispatch()
  const sidebarShow = useSelector(state => state.sidebarShow)

  const toggleSidebar = () => {
    const val = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive'
    dispatch({type: 'set', sidebarShow: val})
  }

  const toggleSidebarMobile = () => {
    const val = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive'
    dispatch({type: 'set', sidebarShow: val})
  }
  //let roles = []
  const access_token = localStorage.getItem('access_token')
  //access_token ? roles = access_token.user.roles.map(item => item.nome) : roles = []

  if(getUser().UserRole == 'Admin'){
    return (
      <>
          <CHeader withSubheader>
              <CToggler
                inHeader
                className="ml-md-3 d-lg-none"
                onClick={toggleSidebarMobile}
              />
              <CToggler
                inHeader
                className="ml-3 d-md-down-none"
                onClick={toggleSidebar}
              />
              <CHeaderBrand className="mx-auto d-lg-none" to="/">
                <CIcon name="logo" height="48" alt="Logo"/>
              </CHeaderBrand>

              <CHeaderNav className="d-md-down-none mr-auto">
                <CTooltip content="Tableau de Bord">
                  <CHeaderNavItem className="px-3" >
                    <CHeaderNavLink to="/dashboard">
                      <CIcon name={'cilSpeedometer'}  />
                    </CHeaderNavLink>
                  </CHeaderNavItem>
                </CTooltip>
                
                    <CTooltip content="Gestion des véhicules">
                    <CHeaderNavItem className="px-3" >
                      <CHeaderNavLink to="/add-car">
                        <CIcon name={'cilBadge'}  />
                      </CHeaderNavLink>
                    </CHeaderNavItem>
                    </CTooltip>
            
                  <CTooltip content="Liste d'Emprunts">
                    <CHeaderNavItem  className="px-3">
                      <CHeaderNavLink to="/liste-emprunts"><CIcon name={'cilCalendar'} /></CHeaderNavLink>
                    </CHeaderNavItem>
                  </CTooltip>
                
                  <CTooltip content="Gestion des Utilisateurs">
                    <CHeaderNavItem className="px-3">
                      <CHeaderNavLink to="/users"><CIcon name={'cilPeople'} /></CHeaderNavLink>
                    </CHeaderNavItem>
                  </CTooltip>
                
                  <CTooltip content="Gestion des sites">
                    <CHeaderNavItem className="px-3">
                      <CHeaderNavLink to="/sites"><CIcon name={'cilHome'} /></CHeaderNavLink>
                    </CHeaderNavItem>
                  </CTooltip>

              </CHeaderNav>

              <CHeaderNav className="px-3">
                <TheHeaderDropdown/>
              </CHeaderNav>

              <CSubheader className="px-3 justify-content-between">
                <CBreadcrumbRouter
                  className="border-0 c-subheader-nav m-0 px-0 px-md-3"
                  routes={routes}
                />
              </CSubheader>
          </CHeader>
      </>
    )
  }else{
    return (
      <>
          <CHeader withSubheader>
              <CToggler
                inHeader
                className="ml-md-3 d-lg-none"
                onClick={toggleSidebarMobile}
              />
              <CToggler
                inHeader
                className="ml-3 d-md-down-none"
                onClick={toggleSidebar}
              />
              <CHeaderBrand className="mx-auto d-lg-none" to="/">
                <CIcon name="logo" height="48" alt="Logo"/>
              </CHeaderBrand>

              <CHeaderNav className="d-md-down-none mr-auto">
                <CTooltip content="Demande d'Emprunt">
                  <CHeaderNavItem className="px-3" >
                    <CHeaderNavLink to="/dashboard">
                      <CIcon name={'cilCalendar'}  />
                    </CHeaderNavLink>
                  </CHeaderNavItem>
                </CTooltip>

                <CTooltip content="Historiques">
                  <CHeaderNavItem className="px-3" >
                    <CHeaderNavLink to="/historiques">
                      <CIcon name={'cilList'}  />
                    </CHeaderNavLink>
                  </CHeaderNavItem>
                </CTooltip>

              </CHeaderNav>

              <CHeaderNav className="px-3">
                <TheHeaderDropdown/>
              </CHeaderNav>

              <CSubheader className="px-3 justify-content-between">
                <CBreadcrumbRouter
                  className="border-0 c-subheader-nav m-0 px-0 px-md-3"
                  routes={routes}
                />
              </CSubheader>
          </CHeader>
      </>
    )
  }
}

export default TheHeader
