import axios, {getUser} from '../services/api'

export default (
  getUser().UserRole == 'Admin' ? [
    {
      _tag: 'CSidebarNavItem',
      name: 'Tableau de Bord',
      to: '/dashboard',
      icon: 'cil-speedometer'
    },
    {
      _tag: 'CSidebarNavItem',
      name: 'Gestion des Véhicules',
      to: '/add-car',
      icon: 'cil-Badge'
    },
    {
      _tag: 'CSidebarNavItem',
      name: 'Liste d\'Emprunts',
      to: '/liste-emprunts',
      icon: 'cil-Calendar'
    },
    {
      _tag: 'CSidebarNavItem',
      name: 'Mes Historiques',
      to: '/historiques',
      icon: 'cil-List'
    },
    {
      _tag: 'CSidebarNavItem',
      name: 'Gestion des Sites',
      to: '/sites',
      icon: 'cil-Home'
    },
    {
      _tag: 'CSidebarNavItem',
      name: 'Gestion des Utilisateurs',
      to: '/users',
      icon: 'cil-People'
    },
    
    {
      _tag: 'CSidebarNavDivider',
      className: 'm-2',
    }
  ] : [
    {
      _tag: 'CSidebarNavItem',
      name: 'Demande d\'Emprunt',
      to: '/dashboard',
      icon: 'cil-Calendar'
    },
    {
      _tag: 'CSidebarNavItem',
      name: 'Historiques',
      to: '/historiques',
      icon: 'cil-List'
    },
  
    {
      _tag: 'CSidebarNavDivider',
      className: 'm-2',
    }
  ]
)

