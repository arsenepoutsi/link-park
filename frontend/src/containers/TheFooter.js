import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter>
      <div className="ms-auto">
        <a href="https://coreui.io" target="_blank" rel="noopener noreferrer">
          LINK'IN PARK
        </a>
        <span className="ms-1">&copy; 2022 NAV-MAT.</span>
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
