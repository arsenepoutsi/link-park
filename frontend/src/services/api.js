import axios from 'axios'

const API_URL = "http://163.172.151.186:5050/api/";
const instance = axios.create({
    baseURL: API_URL
});

/**
 * The complete Triforce, or one or more components of the Triforce.
 * @typedef {Object} JSONWebToken
 * @property {string} UserEmail User email
 * @property {string} UserFullName User fullname
 * @property {string} UserId UserID
 * @property {string} UserRole User role
 * @property {number} exp Expiration date (unix date)
 * @property {string} jti TOKEN ID
 * @property {boolean} tokenIsValid  If true the token is valid
 */

/**
 * @returns JSONWebToken
 */
 function getUser() {
  /** @type JSONWebToken */
  let ret;
  try {
    ret = JSON.parse(atob(localStorage.getItem("access_token").split(".")[1]))
    ret.tokenIsValid = ret.exp >= (Date.now()/1e3);
  } catch (error) {
    ret = {
      UserEmail : "",
      UserFullName : "",
      UserId : "",
      UserRole : "",
      exp : -1,
      jti : "",
      tokenIsValid : false
    };
    // return null;
  }
  return ret;
}

instance.login = async function (values) {
  values = values ||  {
    email : localStorage.email,
    password : localStorage.password
  }
    const response = await instance.post('/Token', { email: values.email, password: values.password }, {
      headers: {'Content-Type': 'application/json'},
      responseType : "text"
    }).then(response => {
      localStorage.setItem('access_token', response.data)
      localStorage.email = values.email;
      localStorage.password = values.password;
      //history.push('/')
      //window.location.href = '/'
    }).catch(err => {
      console.log(err)
      throw response.data.message || err ;
    })
}

instance.getUser = getUser;
instance.interceptors.request.use(
  async config => {
    if(localStorage.hasOwnProperty("access_token")){
      if(!getUser().tokenIsValid){
        // do login here
        await instance.login();
      }
      config.headers['Authorization'] = `Bearer ${localStorage.getItem('access_token').replace(/"/g,"")}`;

    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

export default instance;
export {getUser}
