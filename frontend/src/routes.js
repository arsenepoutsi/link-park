import React from 'react';

const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Historiques = React.lazy(() => import('./views/Historiques'));
const AjoutCar = React.lazy(() => import('./views/AjoutCar'));
const Users = React.lazy(() => import('./views/Users'));
const ListEmprunt = React.lazy(() => import('./views/ListEmprunt'));
const AddSite = React.lazy(() => import('./views/AddSite'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Tableau de Bord', component: Dashboard },
  { path: '/historiques', name: 'Historiques', component: Historiques},
  { path: '/add-car', name: 'Ajouter un Véhicule', component: AjoutCar},
  { path: '/users', name: 'Liste des Utilisateurs', component: Users},
  { path: '/liste-emprunts', name: 'Liste d\'Emprunts', component: ListEmprunt},
  { path: '/sites', name: 'Gestion des Sites', component: AddSite}
];

export default routes;
